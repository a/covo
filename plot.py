import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import datetime, timedelta
import json
import os

hours = mdates.HourLocator()
days = mdates.DayLocator()
dates_fmt = mdates.DateFormatter("%Y-%m-%d")

fig, ax = plt.subplots()

country = "Turkey"

data = {"cases": [[], []], "deaths": [[], []], "recoveries": [[], []]}

lastdate = None
for filename in sorted(os.listdir("data")):
    with open(os.path.join("data", filename)) as f:
        j = json.load(f)

    date = datetime.fromisoformat(j[country]["lastupdate"])

    # TODO: clean this
    if date.date() == lastdate:
        continue
    else:
        lastdate = date.date()

    datenum = mdates.date2num(date)

    for line in data:
        data[line][0].append(datenum)
        data[line][1].append(j[country][line])


for line in data:
    ax.plot(data[line][0], data[line][1], label=line)

ax.set_title(f"COVID-19 chart for {country}")
plt.xticks(rotation=45)
plt.grid(b=True, which='major', color='#d3d3d3', linestyle='-')
ax.xaxis.set_major_locator(days)
ax.xaxis.set_major_formatter(dates_fmt)
ax.xaxis.set_minor_locator(hours)
ax.legend()

plt.tight_layout()
plt.savefig(f"{country}.png")
# plt.show()