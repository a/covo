def check_if_updated(country, old_data, all_data):
    # Account for empty old data
    if country not in old_data:
        return True

    # Go through the important attributes only for update checking
    for attribute in ["cases", "deaths", "recoveries"]:
        if old_data[country][attribute] != all_data[country][attribute]:
            return True
    return False
