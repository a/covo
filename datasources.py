import requests
import time
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import math
import io
import re
import csv


def _cbddo_get_last_update():
    out = requests.get("https://corona.cbddo.gov.tr/")
    isonow = datetime.utcnow().isoformat()

    if out.status_code != 200:
        return isonow

    soup = BeautifulSoup(out.text, "html.parser")
    last_update = soup(text=re.compile("Son Veri Güncelleme Tarihi"))

    if not last_update:
        return isonow

    last_update = last_update[0].strip().split(": ")[-1]
    # TODO: this is hacky, but I blame their shit code moving between multiple time formats
    try:
        iso_last = datetime.strptime(last_update, "%d/%m/%Y %H:%M")
    except ValueError:
        try:
            iso_last = datetime.strptime(last_update, "%m/%d/%Y %I:%M:%S %p")
        except ValueError:
            print(last_update)
            return isonow
    return (iso_last - timedelta(hours=3)).isoformat()


def get_cbddo():
    timestamp = math.floor(time.time() * 1000)
    out = requests.get(f"https://corona.cbddo.gov.tr/Home/GetTotalData2?_={timestamp}")
    outj = out.json()
    data = [a for a in outj["data"] if a["countryStats"]["name"] == "T\u00FCrkiye"][0][
        "countryStats"
    ]

    return {
        "Turkey": {
            "cases": data["confirmedCount"],
            "deaths": data["deathCount"],
            "recoveries": data["recovryCount"],
            "lastupdate": _cbddo_get_last_update(),
            "source": "CBDDO",
        }
    }


def get_jhu():
    # jhu mostly pushes out stuff for that day at roughly end of the day
    # at least in UTC+3.
    # For example, as by the time 27th's data is out it's already 28th
    # it makes more sense to look for 27th's data.
    date = (datetime.utcnow() - timedelta(days=1)).strftime("%m-%d-%Y")
    url = f"https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/{date}.csv"
    out = requests.get(url)

    if out.status_code != 200:
        return {}

    out_data = {}
    with io.StringIO(out.text) as f:
        data = csv.DictReader(f)

        for row in data:
            country = row["Country_Region"]
            if country not in out_data:
                out_data[country] = {"cases": 0, "deaths": 0, "recoveries": 0}

            out_data[country]["cases"] += int(row["Confirmed"])
            out_data[country]["deaths"] += int(row["Deaths"])
            out_data[country]["recoveries"] += int(row["Recovered"])
            out_data[country]["lastupdate"] = row["Last_Update"]
            out_data[country]["source"] = "JHU"
    return out_data
