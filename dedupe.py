import os
import json

lastfile = ""

for filename in sorted(os.listdir("data")):
    if not lastfile:
        lastfile = filename
        continue

    with open(os.path.join("data", lastfile)) as lf:
        lastj = json.load(lf)
    with open(os.path.join("data", filename)) as f:
        curj = json.load(f)

    curj["Turkey"]["lastupdate"] = lastj["Turkey"]["lastupdate"]

    if lastj == curj:
        os.unlink(os.path.join("data", filename))
        print(f"Deleted {filename}")
    else:
        lastfile = filename

