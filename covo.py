import config
import json
import os
from datetime import datetime
import discordapi

telegram_enabled = bool(config.tg_channels)

if telegram_enabled:
    import telegramapi


# Load old state to see if any data is updated yet or not
if os.path.exists("state.json"):
    with open("state.json") as f:
        old_data = json.load(f)
else:
    old_data = {}

all_data = {}
updated_countries = []
for country in config.check_countries:
    data_source = config.sources.get(country, config.default_source)
    # Don't get from default source multiple times if we already pulled from it
    if country in all_data and data_source == config.default_source:
        continue
    new_data = data_source()
    all_data = {**all_data, **new_data}

discordapi.send_all(old_data, all_data)

if telegram_enabled:
    telegramapi.send_all(old_data, all_data)

# Merge old and all data
all_data = {**old_data, **all_data}
with open("state.json", "w") as f:
    json.dump(all_data, f)


if all_data != old_data:
    # Save historic data for analysis and debug purposes
    os.makedirs("data", exist_ok=True)
    filename = datetime.utcnow().isoformat() + ".json"
    with open(os.path.join("data", filename), "w") as f:
        json.dump(all_data, f)
