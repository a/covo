# CoVo

Discord webhook for posting updates on COVID-19 statistics.

## Installation
- Install python3.6+
- Install requirements in `requirements.txt`
- Copy `config.template.py` to `config.py`
- Configure `config.py` by adding your own countries, webhook URLs etc
- Run `covo.py`. You might want to set up a cron entry or a systemd timer to do this every day or so to keep posting updates.

## Screenshots

![](https://elixi.re/i/em13qjbi.png)

When there's new cases (the "new" numbers below are test numbers):

![](https://elixi.re/i/19l2zlo6.png)

