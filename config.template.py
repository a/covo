from datasources import *

default_source = get_jhu
sources = {"Turkey": get_cbddo}

# For countries that have custom sources set in sources dict above
# put them after generic sources like get_jhu
check_countries = {"US": "us", "Italy": "it", "Turkey": "tr"}

notify = {
    "webhook url goes here": {"text": "", "countries": ["Turkey", "US", "Italy"],}
}

# Telegram config
# to disable telegram support, leave tg_channels empty
tg_api_id = 0
tg_api_hash = ""
tg_channels = []
