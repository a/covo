import config
from telethon.sync import TelegramClient
from common import check_if_updated


def send_all(old_data, all_data):
    for tg in config.tg_channels:
        tg_data = config.tg_channels[tg]
        tg_text = []
        for country in tg_data["countries"]:
            # Don't attempt to post data if it's not available
            if country not in all_data:
                continue

            # Don't repost
            if not check_if_updated(country, old_data, all_data):
                continue

            tg_text.append(create_message(country, old_data, all_data))

        # If there's no texts, then don't send a useless message
        if not tg_text:
            continue

        send_message(tg, tg_data["text"] + "\n".join(tg_text))


def create_message(country, old_data, all_data):
    cases = all_data[country]["cases"]
    deaths = all_data[country]["deaths"]
    recoveries = all_data[country]["recoveries"]

    active_cases = cases - deaths - recoveries

    per = lambda a, b: round((a / b) * 100, 2) if b > 0 else 0

    death_percentage = per(deaths, cases)
    recovery_percentage = per(recoveries, cases)

    case_str = f"{cases} ({active_cases} active"
    death_str = f"{deaths} ({death_percentage}%)"
    recovery_str = f"{recoveries} ({recovery_percentage}%)"

    if country in old_data:
        old_cases = old_data[country]["cases"]
        old_deaths = old_data[country]["deaths"]
        old_recoveries = old_data[country]["recoveries"]

        old_active = old_cases - old_deaths - old_recoveries

        case_diff = cases - old_cases
        death_diff = deaths - old_deaths
        recovery_diff = recoveries - old_recoveries
        active_diff = active_cases - old_active

        case_str += f" ({per(active_diff, old_active):+}%)) ({case_diff} new cases ({per(case_diff, old_cases):+}%))" if case_diff else ""
        death_str += f" ({death_diff} new deaths ({per(death_diff, old_deaths):+}%))" if death_diff else ""
        recovery_str += f" ({recovery_diff} new recoveries ({per(recovery_diff, old_recoveries):+}%))" if recovery_diff else ""
    else:
        case_str += ")"

    return f"""
**COVID-19 Statistics for {country}:**

**Cases:** {case_str}
**Deaths:** {death_str}
**Recoveries:** {recovery_str}

Last update: {all_data[country]["lastupdate"]}
Data source: {all_data[country]["source"]}
"""


def send_message(channel, text):
    with TelegramClient("covo", config.tg_api_id, config.tg_api_hash) as client:
        client.get_dialogs()
        client.send_message(channel, text)
