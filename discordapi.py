import config
import requests
from common import check_if_updated


def send_all(old_data, all_data):
    for webhook in config.notify:
        webhook_data = config.notify[webhook]
        embeds = []
        for country in webhook_data["countries"]:
            # Don't attempt to post data if it's not available
            if country not in all_data:
                continue

            # Don't repost
            if not check_if_updated(country, old_data, all_data):
                continue

            embeds.append(create_embed(country, old_data, all_data))

        # If there's no embeds, then don't send a useless webhook
        if not embeds:
            continue

        send_webhook(webhook, webhook_data["text"], embeds)


def create_embed(country, old_data, all_data):
    # TODO: the ISO thing is hacky
    iso_country = config.check_countries[country]
    cases = all_data[country]["cases"]
    deaths = all_data[country]["deaths"]
    recoveries = all_data[country]["recoveries"]

    active_cases = cases - deaths - recoveries

    per = lambda a, b: round((a / b) * 100, 2) if b > 0 else 0

    death_percentage = per(deaths, cases)
    recovery_percentage = per(recoveries, cases)

    case_str = f"{cases} ({active_cases} active"
    death_str = f"{deaths} ({death_percentage}%)"
    recovery_str = f"{recoveries} ({recovery_percentage}%)"

    if country in old_data:
        old_cases = old_data[country]["cases"]
        old_deaths = old_data[country]["deaths"]
        old_recoveries = old_data[country]["recoveries"]

        old_active = old_cases - old_deaths - old_recoveries

        case_diff = cases - old_cases
        death_diff = deaths - old_deaths
        recovery_diff = recoveries - old_recoveries
        active_diff = active_cases - old_active

        case_str += f" ({per(active_diff, old_active):+}%)) ({case_diff} new cases ({per(case_diff, old_cases):+}%))" if case_diff else ""
        death_str += f" ({death_diff} new deaths ({per(death_diff, old_deaths):+}%))" if death_diff else ""
        recovery_str += f" ({recovery_diff} new recoveries ({per(recovery_diff, old_recoveries):+}%))" if recovery_diff else ""
    else:
        case_str += ")"

    return {
        "title": f":flag_{iso_country}: COVID-19 Statistics for {country}",
        "fields": [
            {"name": "Cases", "value": case_str, "inline": True,},
            {"name": "Deaths", "value": death_str, "inline": True,},
            {"name": "Recoveries", "value": recovery_str, "inline": True,},
        ],
        "timestamp": all_data[country]["lastupdate"],
        "footer": {"text": f"Data source: {all_data[country]['source']}"},
    }


def send_webhook(url, text, embeds):
    for i in range(0, len(embeds), 5):
        _send_webhook(url, text, embeds[i : i + 5])


def _send_webhook(url, text, embeds):
    requests.post(url, json={"content": text, "embeds": embeds})
